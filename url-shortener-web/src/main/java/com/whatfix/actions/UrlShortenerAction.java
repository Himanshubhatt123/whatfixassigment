package com.whatfix.actions;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.whatfix.domain.ShortUrl;
import com.whatfix.services.UrlShortenerService;

public class UrlShortenerAction {
   private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerAction.class);
   private UrlShortenerService urlShortenerService;
   private ShortUrl shortUrl;
   private List<String> violations = new ArrayList<String>();

   public void setUrlShortenerService(UrlShortenerService urlShortenerService) {
      this.urlShortenerService = urlShortenerService;
   }

   public ShortUrl getShortUrl() {
      return shortUrl;
   }

   public void setShortUrl(ShortUrl shortUrl) {
      this.shortUrl = shortUrl;
   }

   public List<String> getViolations() {
      return this.violations;
   }

   public String execute() {
      String returnCode = "ERROR";

      if(shortUrl != null) {

         ShortUrl duplicateShortUrl = urlShortenerService.wasUrlAlreadyShortened(shortUrl.getFullUrl());

         if(duplicateShortUrl == null) {
            violations = urlShortenerService.validateShortUrl(shortUrl);

            if(violations.isEmpty()) {
               String errorMessage = urlShortenerService.saveUrl(shortUrl);
               if(StringUtils.isBlank(errorMessage)) {
                  returnCode = "SUCCESS";
               } else {
                  violations.add(errorMessage);
                  returnCode = "ERROR";
               }
            } else {
               LOGGER.debug("Did not save due to validation errors: " + violations);
            }
         } else {
            shortUrl = duplicateShortUrl;
            LOGGER.debug("URL had already been shortened: " + shortUrl);
            returnCode = "SUCCESS";
         }

      } else {
         violations.add("No URL to save");
      }
      return returnCode;
   }
}
