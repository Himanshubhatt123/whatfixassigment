package com.whatfix.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.Preparable;

import com.whatfix.services.UrlShortenerService;


public class UrlExpandAction implements Preparable {
   private static final Logger LOGGER = LoggerFactory.getLogger(UrlExpandAction.class);

   private UrlShortenerService urlShortenerService;
   private String shortUrl;
   private String fullUrl;
private static String getActionName() {
      return ServletActionContext.getActionMapping().getName();
   }

   @Override
   public void prepare() {
      this.shortUrl = UrlExpandAction.getActionName();
   }

   public void setUrlShortenerService(UrlShortenerService urlShortenerService) {
      this.urlShortenerService = urlShortenerService;
   }

   public String getShortUrl() {
      return this.shortUrl;
   }

   public void setShortUrl(String shortUrl) {
      this.shortUrl = shortUrl;
   }

   public String getFullUrl() {
      return this.fullUrl;
   }

   public void setFullUrl(String fullUrl) {
      this.fullUrl = fullUrl;
   }

   public String execute() {
      LOGGER.debug("My action name (the shortened URL) is: " + shortUrl);
      this.fullUrl = urlShortenerService.expandShortUrl(shortUrl);
      return (this.fullUrl != null && this.fullUrl.length() > 0) ? "SUCCESS" : "ERROR";
   }
}