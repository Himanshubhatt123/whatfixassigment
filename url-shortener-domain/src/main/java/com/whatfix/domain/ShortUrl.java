package com.whatfix.domain;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import com.whatfix.url.algorithms.AlphaNumericCodec;
import com.whatfix.url.algorithms.Codec;

@Entity
public class ShortUrl implements Serializable {

   private static final long serialVersionUID = 7827630535667254219L;
   
   private static final Codec CODEC = new AlphaNumericCodec();
   private static final int MAX_URL_LENGTH = 500;
   
  
   private static int safeLongToInt(long l) {
      if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
         throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
      }
      return (int) l;
   }
   
   public static Long shortCodeToId(String shortCode) {
	   int i = CODEC.decode(shortCode);
	   return new Long(i);
   }

   @Id
   @GeneratedValue(strategy = AUTO)
   private Long id;

   @NotBlank(message = "Full URL {javax.validation.constraints.NotBlank.message}")
   @Size(max = MAX_URL_LENGTH, message="Full URL {javax.validation.constraints.Size.message}")
   @URL
   private String fullUrl;

   public Long getId() {
      return this.id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getFullUrl() {
      return this.fullUrl;
   }

   public void setFullUrl(String fullUrl) {
      this.fullUrl = fullUrl;
   }

   @Transient
   public String getShortUrl() {
      // FIXME precision can be lost
      int integerId = safeLongToInt(this.getId());
      return CODEC.encode(integerId);
   }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
