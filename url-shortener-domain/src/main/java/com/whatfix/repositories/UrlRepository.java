package com.whatfix.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.whatfix.domain.ShortUrl;


public interface UrlRepository extends PagingAndSortingRepository<ShortUrl, Long> {
   ShortUrl findByFullUrl(String fullUrl);
}
