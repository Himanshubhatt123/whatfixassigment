package com.whatfix.url.algorithms;
public class AlphaNumericCodec implements Codec {
	
	private static final String ALPHABET = 
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	private static final int ALPHABET_LENGTH = ALPHABET.length();
	private static final char[] CHARSET = ALPHABET.toCharArray();
	
	public String encode(int number) {
		int i = number;
		if(i == 0) {
			return Character.toString(CHARSET[0]);
		}
		
		StringBuilder stringBuilder = new StringBuilder();
		while(i > 0) {
			int remainder = i % ALPHABET_LENGTH;
			i /= ALPHABET_LENGTH;
			stringBuilder.append(CHARSET[remainder]);
		}
		return stringBuilder.reverse().toString();
	}

	@Override
	public int decode(String s) {
		int i = 0;
		char[] chars = s.toCharArray();
		for(char c : chars) {
			i = i * ALPHABET_LENGTH + ALPHABET.indexOf(c);
		}
		return i;
	}

}
