
package com.whatfix.url.algorithms;
public interface Codec {
	String encode(int i);
	int decode(String s);
}
