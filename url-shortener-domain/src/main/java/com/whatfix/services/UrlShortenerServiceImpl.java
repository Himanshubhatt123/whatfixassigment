package com.whatfix.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionException;

import com.whatfix.domain.ShortUrl;
import com.whatfix.repositories.UrlRepository;
public class UrlShortenerServiceImpl implements UrlShortenerService {

   private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerServiceImpl.class);

   @Autowired
   private UrlRepository urlRepository;

   @Autowired
   private Validator validator;

   public ShortUrl wasUrlAlreadyShortened(String fullUrl) {
      return urlRepository.findByFullUrl(fullUrl);
   }

   public List<String> validateShortUrl(ShortUrl shortUrl) {
      List<String> violations = new ArrayList<String>();
      Set<ConstraintViolation<ShortUrl>> constraintViolations = validator.validate(shortUrl);
      for(ConstraintViolation<ShortUrl> constraintViolation : constraintViolations) {
         violations.add(constraintViolation.getMessage());
      }
      return violations;
   }

   public String saveUrl(ShortUrl shortUrl) {
      LOGGER.trace("trying to save URL...");
      String errorMessage = "";

      try {
         urlRepository.save(shortUrl);

      } catch(DataAccessException ex) {
         LOGGER.error("DataAccessException when saving ShortUrl", ex);
         errorMessage = ex.getMessage();
      } catch(TransactionException ex) {
         LOGGER.error("TransactionException when saving ShortUrl", ex);
         errorMessage = ex.getMessage();
      } catch(RuntimeException ex) {
         LOGGER.error("RuntimeException when saving ShortUrl", ex);
         errorMessage = ex.getMessage();
      }
      return errorMessage;
   }

   public Iterable<ShortUrl> findAll() {
      return urlRepository.findAll();
   }

   public String expandShortUrl(String shortCode) {
      Long id = ShortUrl.shortCodeToId(shortCode);
      ShortUrl shortUrl = urlRepository.findOne(id);
      return (shortUrl != null) ? shortUrl.getFullUrl() : "";
   }

}
