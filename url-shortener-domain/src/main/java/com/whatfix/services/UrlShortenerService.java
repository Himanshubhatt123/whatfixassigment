package com.whatfix.services;

import java.util.List;

import com.whatfix.domain.ShortUrl;
public interface UrlShortenerService {

   ShortUrl wasUrlAlreadyShortened(String fullUrl);
   List<String> validateShortUrl(ShortUrl shortUrl);
   String saveUrl(ShortUrl shortUrl);
   Iterable<ShortUrl> findAll();
   String expandShortUrl(String shortUrl);
}
